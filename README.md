# SIPPS

Sistema de Información para la Planeación de los Paisajes Sostenibles Provincia de Oxapampa Perú.

## Structure

Se presenta toda la [estructura](structure.md) del sitio web, mencionando las páginas que lo conforman.

## License

[MIT](LICENCE) © [Francisco M. L.](https://gitlab.com/FranciscoML)
